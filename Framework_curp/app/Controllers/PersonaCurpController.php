<?php

require 'app/Models/Conexion.php';
require 'app/Models/Curp.php';
use curp\Curp;
use curp\Conexion;
class PersonaCurpController
{

    public function __construct()
    {
        if($_GET["action"] == "perfil"){
            if(!isset($_SESSION["loginEntra"])){
                //no sea iniciado sesion y redireccionara a la vista login
                echo "No has iniciado sesion";
                header("location:/repo/Framework_curp/index.php?controller=PersonaCurp&action=login");
            }
        }
    }

    function verificaDatosInicio(){

        // -> recibe variable de vista.
        // ->pasar fechaNacimiento a clase Curp.
        $persona = new Curp();
        if (!isset($_POST["primer_nombreE"]) || !isset($_POST["segundo_nombreE"])
            || !isset($_POST["apellido_paternoE"]) || !isset($_POST["apellido_maternoE"])
            || !isset($_POST["diaE"]) || !isset($_POST["mesE"]) || !isset($_POST["anioE"])
            || !isset($_POST["generoE"]) || !isset($_POST["estado_nacimientoE"])){

        }else{
            $persona->primer_nombre = $_POST["primer_nombreE"];
            $persona->segundo_nombre = $_POST["segundo_nombreE"];
            $persona->apellido_paterno = $_POST["apellido_paternoE"];
            $persona->apellido_materno = $_POST["apellido_maternoE"];
            $persona->dia=$_POST["diaE"];
            $persona->mes = $_POST["mesE"];
            $persona->anio = $_POST["anioE"];
            $persona->genero = $_POST["generoE"];
            $persona->estado_nacimiento = $_POST["estado_nacimientoE"];
            echo "<br>   Su curp es:  ".$persona->curpCompleta()."<br>"."<br>";

        }
        require "app/views/FormCurp.php";



    }

    function login(){

        require 'app/Views/login.php';
        $this->verificaDatosLogin();

    }
    function verificaDatosLogin(){

        if (!isset($_POST["correo"]) || !isset($_POST["pass"])){

        }else{
            $correo = $_POST["correo"];
            $contrasenia = $_POST["pass"];
            $verificar = Curp::VerificarLogin($correo, $contrasenia);
            // $verificar->"nombre del atributo a mostrar"
            // echo json_encode($verificar); <- Imprimer el objeto completo. Muestra los valores de los atributos de la clase.
            if (!$verificar){

                $estatus = "Datos Incorrectos";
                require 'app/Views/login.php';

            }else{
                $_SESSION["loginEntra"] = $verificar;
                $this->perfil();
            }
        }
    }

    public function logout(){

        if(isset($_SESSION["loginEntra"])){
            unset($_SESSION["loginEntra"]);
        }
        echo "sesion cerrada";
    }

    function registro(){

        require 'app/Views/registro.php';
        $this->verificaDatosRegistro();

    }
    function verificaDatosRegistro(){

        // -> recibe variable de vista registro.
        /* pedir todas las variables de la clase curp añadiendo tambien
           los atributos correo y contraseña a la clase
           y añadiendolos a la BD tambien para guardarlos.
        */

        $registroPersona = new Curp();
        if (!isset($_POST["primer_nombreReg"]) || !isset($_POST["segundo_nombreReg"])
            || !isset($_POST["apellido_paternoReg"]) || !isset($_POST["apellido_maternoReg"])
            || !isset($_POST["diaReg"]) || !isset($_POST["mesReg"]) || !isset($_POST["anioReg"])
            || !isset($_POST["generoReg"]) || !isset($_POST["estado_nacimientoReg"])
            || !isset($_POST["correoReg"]) || !isset($_POST["contraseniaReg"])){

        }else{

            $registroPersona->primer_nombre = $_POST["primer_nombreReg"];
            $registroPersona->segundo_nombre = $_POST["segundo_nombreReg"];
            $registroPersona->apellido_paterno = $_POST["apellido_paternoReg"];
            $registroPersona->apellido_materno = $_POST["apellido_maternoReg"];
            $registroPersona->dia=$_POST["diaReg"];
            $registroPersona->mes = $_POST["mesReg"];
            $registroPersona->anio = $_POST["anioReg"];
            $registroPersona->genero = $_POST["generoReg"];
            $registroPersona->estado_nacimiento = $_POST["estado_nacimientoReg"];
            $registroPersona->correo = $_POST["correoReg"];
            $registroPersona->contrasenia = $_POST["contraseniaReg"];
            $registroPersona->registraUsuario();
            $id = Curp::extraer_id($_POST["correoReg"]);
            $curpCompleta = $registroPersona->curpCompleta();
            // metodo enviar curp y id a tabla persona_curp
            //metodo($id,$curp);
            $registroPersona->mandarIdYCurp($id->id_persona,$curpCompleta);
            $this->perfil();
            echo "Su curp es: ".$registroPersona->curpCompleta();
        }


    }
    function perfil(){

        // en vista perfil abrir un espacio php para mandar a llamar al metodo
        // "consultaDatosPersonaCurp" de la clase Curp.

        if(isset($_POST["correoReg"])){
            $id = Curp::extraer_id($_POST["correoReg"]);
            $datosPersona = Curp::consultaDatosPersonaCurp($id->id_persona);
        }else{
            $correo = $_POST["correo"];
            $contrasenia = $_POST["pass"];
            $verificar = Curp::VerificarLogin($correo, $contrasenia);
            $datosPersona = Curp::consultaDatosPersonaCurp($verificar->id_persona);
        }
        require 'app/Views/perfil.php';

    }

}