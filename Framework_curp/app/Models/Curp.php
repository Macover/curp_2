<?php


namespace curp;

class Curp extends Conexion
{
    public $id;
    public $primer_nombre;
    public $segundo_nombre;
    public $apellido_paterno;
    public $apellido_materno;
    public $dia;
    public $mes;
    public $anio;
    public $genero;
    public $estado_nacimiento;
    public $correo;
    public $contrasenia;



    public function __construct()
    {
        parent::__construct();
    }
    /*
    function registrar(){
        $pre = mysqli_prepare($this->conexion, "INSERT INTO persona(primer_nombre,segundo_nombre,apellido_paterno,apellido_materno,dia,mes,anio,genero,estado_nacimiento) VALUES  (?,?,?,?,?,?,?,?,?)");
        $pre->bind_param("ssssiiiss", $this->primer_nombre,$this->segundo_nombre, $this->apellido_paterno,$this->apellido_materno,$this->dia,$this->mes,$this->anio,$this->genero, $this->estado_nacimiento);
        $pre->execute();
    }
    */

    function getPrimeraLetraApellidoP(){

        $getPrimeraLetraApellidoP = substr($this->apellido_paterno,0,1);
        return $getPrimeraLetraApellidoP;

    }
    function getPrimeraVocalApellidoP(){

        $vocales = array_intersect(str_split($this->apellido_paterno), array('A','E','I','O','U','a','e','i','o','u'));
        //$getPrimeraVocalApellidoP = substr($this->apellido_paterno,0,1);
        return array_shift($vocales);
    }
    function getPrimeraLetraApellidoM(){

        $getPrimeraLetraApellidoM = substr($this->apellido_materno,0,1);
        return $getPrimeraLetraApellidoM;

    }
    function getPrimeraLetraPrimerNombre(){

        $getPrimeraLetraPrimerNombre = substr($this->primer_nombre,0,1);
        return $getPrimeraLetraPrimerNombre;

    }
    function getAnio(){
        $getAnio = substr($this->anio,-2);
        return $getAnio;
    }
    function getSegundaConsonanteApellidoP(){

        $consonantes = array_intersect(str_split($this->apellido_paterno), array('b','c', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 'n', 'ñ', 'p', 'q', 'r', 's', 't', 'v', 'x', 'y', 'z','B','C', 'D', 'F', 'G', 'H', 'J', 'K', 'L', 'M', 'N', 'Ñ', 'P', 'Q', 'R', 'S', 'T', 'V', 'X', 'Y', 'Z'));
        $arreglo = array_values($consonantes);
        return $arreglo[1];

    }
    function getSegundaConsonanteApellidoM(){

        $consonantes = array_intersect(str_split($this->apellido_materno), array('b','c', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 'n', 'ñ', 'p', 'q', 'r', 's', 't', 'v', 'x', 'y', 'z','B','C', 'D', 'F', 'G', 'H', 'J', 'K', 'L', 'M', 'N', 'Ñ', 'P', 'Q', 'R', 'S', 'T', 'V', 'X', 'Y', 'Z'));
        $arreglo = array_values($consonantes);
        return $arreglo[1];

    }
    function getSegundaConsonantePrimerNombre(){

        $consonantes = array_intersect(str_split($this->primer_nombre), array('b','c', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 'n', 'ñ', 'p', 'q', 'r', 's', 't', 'v', 'x', 'y', 'z','B','C', 'D', 'F', 'G', 'H', 'J', 'K', 'L', 'M', 'N', 'Ñ', 'P', 'Q', 'R', 'S', 'T', 'V', 'X', 'Y', 'Z'));

        if(count($consonantes)==1){
            return array_shift($consonantes);
        }else{
            $arreglo = array_values($consonantes);
            return $arreglo[1];
        }
       // agregar funcion if
    }

    function curpCompleta(){

        return strtoupper($this->getPrimeraLetraApellidoP().$this->getPrimeraVocalApellidoP().$this->getPrimeraLetraApellidoM()
        .$this->getPrimeraLetraPrimerNombre().$this->getAnio().$this->mes.$this->dia.$this->genero.$this->estado_nacimiento
            .$this->getSegundaConsonanteApellidoP().$this->getSegundaConsonanteApellidoM().$this->getSegundaConsonantePrimerNombre() ."A8");
    }

    static function VerificarLogin($correo, $contrasenia){

        $conexion = new Conexion();
        // remplazar nombre de tabla por: "persona" cuando la vista registro este funcionando.
        $pre = mysqli_prepare($conexion->conexion,"SELECT * FROM registros WHERE correo = ? 
        AND contrasenia = ?");
        $pre->bind_param("ss",$correo, $contrasenia);
        $pre->execute();
        $resultado = $pre->get_result();
        return $resultado->fetch_object();

    }

    function registraUsuario(){
        $prepara = mysqli_prepare($this->conexion, "INSERT INTO registros(primer_nombre,segundo_nombre,apellido_paterno,apellido_materno,dia,mes,anio,genero,estado_nacimiento,correo,contrasenia) VALUES  (?,?,?,?,?,?,?,?,?,?,?)");
        $prepara->bind_param("ssssiiissss", $this->primer_nombre,$this->segundo_nombre, $this->apellido_paterno,$this->apellido_materno,$this->dia,$this->mes,$this->anio,$this->genero, $this->estado_nacimiento,$this->correo, $this->contrasenia);
        $prepara->execute();
    }

    static function extraer_id($correo){

        $conexion = new Conexion();
        $pre = mysqli_prepare($conexion->conexion,"SELECT id_persona FROM registros WHERE correo = ? ");
        $pre->bind_param("s",$correo);
        $pre->execute();
        $resultado = $pre->get_result();
        return $resultado->fetch_object();
    }


    function mandarIdYCurp($id,$curp){

        $pre = mysqli_prepare($this->conexion, "INSERT INTO persona_curp(id_persona,curp_completa) VALUES (?,?)");
        $pre->bind_param("ss", $id,$curp);
        $pre->execute();

    }

    static function consultaDatosPersonaCurp($id_persona){

        // metodo que pedira los datos a la bd de la persona registrada para mostrarlos en la vista perfil.
        // .
        $conexion = new Conexion();
        $pre = mysqli_prepare($conexion->conexion,"SELECT * FROM registros WHERE id_persona = ?");
        $pre->bind_param("s",$id_persona);
        $pre->execute();
        $resultado = $pre->get_result();
        return $resultado->fetch_object();


    }

}