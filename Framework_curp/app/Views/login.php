<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="public/css/estilosLogin.css">
    <title>Login</title>
</head>
<body>

        <center>
            <?php
            if(isset($estatus)) {
                echo $estatus;
            }
            ?>
            <h1>Login</h1><br>
            <form method="post" action="index.php?controller=PersonaCurp&action=verificaDatosLogin">
                <label>Correo: </label><br>
                <input type="text" name="correo" required>
                <br>
                <label>Contraseña:</label><br>
                <input type="password" name="pass" required>
                <br>
                <input type="submit" value="Enviar">
            </form>
        </center>
</body>
</html>