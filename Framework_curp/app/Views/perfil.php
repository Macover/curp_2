<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="public/css/estilosPerfil.css">
    <title>Mi Perfil</title>
</head>
<body>

     <h1>Mis datos de la curp</h1>

    <div>
        <label> Nombre: </label><br>
        <input class="formInput" type="text" value="<?php echo strtoupper($datosPersona->primer_nombre)?>" disabled><br>
        <label> Segundo nombre: </label><br>
        <input type="text" value="<?php echo strtoupper($datosPersona->segundo_nombre)?>" disabled><br>
        <label> Apellido paterno: </label><br>
        <input type="text" value="<?php echo strtoupper($datosPersona->apellido_paterno)?>" disabled><br>
        <label> Apellido materno: </label><br>
        <input type="text" value="<?php echo strtoupper($datosPersona->apellido_materno)?>" disabled><br>
        <label> Fecha de nacimiento: </label><br>
        <input type="text" value="<?php echo strtoupper($datosPersona->dia."/".$datosPersona->mes."/".$datosPersona->anio)?>" disabled><br>
        <label> Genero: </label><br>
        <input type="text" value="<?php echo strtoupper($datosPersona->genero) ?>" disabled><br>
        <label> Entidad de nacimiento: </label><br>
        <input type="text" value="<?php echo strtoupper($datosPersona->estado_nacimiento)?>" disabled><br>
        <label> Correo: </label><br>
        <input type="text" value="<?php echo $datosPersona->correo?>" disabled><br>
        <label> Contraseña: </label><br>
        <input type="text" value="<?php echo $datosPersona->contrasenia?>" disabled><br>


    </div>


</body>
</html>